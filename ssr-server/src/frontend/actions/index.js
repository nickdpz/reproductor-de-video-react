import axios from 'axios';

export const setFavorite = (payload) => ({
  type: 'SET_FAVORITE',
  payload,
});

export const deleteFavorite = (payload) => ({
  type: 'DELETE_FAVORITE',
  payload,
});

export const loginRequest = (payload) => ({
  type: 'LOGIN_REQUEST',
  payload,
});

export const logoutRequest = (payload) => ({
  type: 'LOGOUT_REQUEST',
  payload,
});

export const setError = (payload) => ({
  type: 'SET_ERROR',
  payload,
});

export const registerRequest = (payload) => ({
  type: 'REGISTER_REQUEST',
  payload,
});

export const getVideoSource = (payload) => ({
  type: 'GET_VIDEO_SOURCE',
  payload,
});

export const deleteFavoriteMovie = (payload) => {
  return (dispatch) => {
    axios({
      url: `/user-movies${payload}`,
      method: 'delete',
    })
      .then(({ data, status }) => {
        if (status === 200) {
          dispatch(deleteFavorite(payload));
          const { message } = data;
          console.log(message);
        } else {
          dispatch(setError({ error: new Error('Bad request') }));
        }
      })
      .catch((err) => dispatch(setError(err)));
  };
};

export const setFavoriteMovie = (userId, payload) => {
  return (dispatch) => {
    axios({
      url: '/user-movies',
      method: 'post',
      data: { userId, movieId: payload.id },
    })
      .then(({ data, status }) => {
        if (status === 201) {
          dispatch(setFavorite({ ...payload, id: data.data }));
          const { message } = data;
          console.log(message);
        } else {
          dispatch(setError({ error: new Error('Bad request') }));
        }
      })
      .catch((err) => dispatch(setError(err)));
  };
};

export const registerUser = (payload, redirectUrl) => {
  return (dispatch) => {
    axios.post({
      url: '/auth/sign-up',
      headers: { 'Content-type': 'application/json' },
      data: payload,
    })
      .then(({ data }) => dispatch(registerRequest(data)))
      .then(() => {
        window.location.href = redirectUrl;
      })
      .catch((err) => dispatch(setError(err)));
  };
};

export const loginUser = ({ email, password }, redirectUrl) => {
  return (dispatch) => {
    axios({
      url: '/auth/sign-in/',
      method: 'post',
      headers: { 'Content-type': 'application/json' },
      auth: {
        username: email,
        password,
      },
    })
      .then(({ data }) => {
        document.cookie = `email=${data.user.email}`;
        document.cookie = `name=${data.user.name}`;
        document.cookie = `id=${data.user.id}`;
        dispatch(loginRequest(data.user));
      })
      .then(() => {
        window.location.href = redirectUrl;
      })
      .catch((err) => dispatch(setError(err)));
  };
};
