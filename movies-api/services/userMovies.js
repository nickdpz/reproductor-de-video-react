const MongoLib = require('../lib/mongo');
const ObjectId = require('mongodb').ObjectID;

class UserMoviesService {
  constructor() {
    this.collection = 'user-movies';
    this.mongoDB = new MongoLib();
  }

  async getUserMovies({ userId }) {
    let userMovies = await this.mongoDB.getAll(this.collection, { userId: ObjectId(userId) });
    let out = [];
    let aux = {};
    for (const userMovie of userMovies) {
      aux = await this.mongoDB.get('movies', userMovie.movieId);
      out.push(aux);
    }
    return out || [];
  }

  async createUserMovie({ userId, movieId }) {
    const createdUserMovieId = await this.mongoDB.create(
      this.collection,
      { userId: ObjectId(userId), movieId: ObjectId(movieId) }
    );

    return createdUserMovieId;
  }

  async deleteUserMovie({ userMovieId }) {
    const deletedUserMovieId = await this.mongoDB.delete(
      this.collection,
      userMovieId
    );

    return deletedUserMovieId;
  }
}

module.exports = UserMoviesService;